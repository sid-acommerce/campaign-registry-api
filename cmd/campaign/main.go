package main

import (
	"log"

	"bitbucket.org/sid-acommerce/campaign-registry-api/internal/config"
	"bitbucket.org/sid-acommerce/campaign-registry-api/internal/infrastructure/datastore"
	"bitbucket.org/sid-acommerce/campaign-registry-api/internal/infrastructure/router"
	"bitbucket.org/sid-acommerce/campaign-registry-api/internal/registry"
	"github.com/joho/godotenv"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	_ "github.com/lib/pq"
)

func init() {
	if err := godotenv.Load(".env.local"); err != nil {
		log.Fatal("Error loading specified env file")
	}
}

func main() {
	cfg := config.NewConfig()
	db := datastore.NewDB(cfg.DB)
	defer db.Close()

	r := registry.NewRegistry(db)

	e := echo.New()
	e = router.NewRouter(e, r.NewAppController())
	e.Debug = true
	e.Pre(middleware.RemoveTrailingSlash())
	e.Logger.Fatal(e.Start(":1323"))
}
