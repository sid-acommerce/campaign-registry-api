module bitbucket.org/sid-acommerce/campaign-registry-api

go 1.13

require (
	github.com/ericlagergren/decimal v0.0.0-20191018222636-98d6b4cb4b5e // indirect
	github.com/friendsofgo/errors v0.9.2
	github.com/gofrs/uuid v3.2.0+incompatible // indirect
	github.com/joho/godotenv v1.3.0
	github.com/kat-co/vala v0.0.0-20170210184112-42e1d8b61f12
	github.com/labstack/echo/v4 v4.1.11
	github.com/lib/pq v1.2.0
	github.com/spf13/viper v1.5.0
	github.com/volatiletech/inflect v0.0.0-20170731032912-e7201282ae8d // indirect
	github.com/volatiletech/null v8.0.0+incompatible
	github.com/volatiletech/sqlboiler v3.6.1+incompatible
	golang.org/x/crypto v0.0.0-20191122220453-ac88ee75c92c // indirect
)
