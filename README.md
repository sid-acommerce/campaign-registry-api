campaign-registry-api
=====================

Campaign Registry is a part of aCommerce Sales Campaign Management. It is designed to capture the definition, intent and activities related to a sales campaign.

### Campaign Logical Structure
* https://acommerce.atlassian.net/wiki/x/OQD9Og


# Clean Architecture
* https://github.com/manakuro/golang-clean-architecture/
* https://github.com/bxcodec/go-clean-arch/
