package controller

import (
	"bitbucket.org/sid-acommerce/campaign-registry-api/internal/campaign"

	"bitbucket.org/sid-acommerce/campaign-registry-api/internal/activity"
)

// AppController unifies all controllers avaialble within the application.
type AppController struct {
	Campaign campaign.CampaignController
	Activity activity.ActivityController
}
