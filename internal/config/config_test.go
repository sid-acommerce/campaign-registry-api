package config

import (
	"os"
	"strconv"
	"testing"
)

func TestGetEnv(t *testing.T) {
	key := "R@NDO3_S3RCS@(*I9jK_STR_ENV"
	expectedVal := "Aleatoria"
	os.Setenv(key, expectedVal)

	actualVal := getEnv(key, "")

	if actualVal != expectedVal {
		t.Errorf("getEnv(\"%s\", \"MacOS\") FAILED, expected \"%s\" but got value \"%s\"", key, expectedVal, actualVal)
	}
}

func TestGetEnvAndGotDefault(t *testing.T) {
	key := "R@NDO3_S3RCS@(*I9jK_STR_ENV"
	os.Unsetenv(key)
	expectedVal := "Random"

	actualVal := getEnv(key, expectedVal)

	if actualVal != expectedVal {
		t.Errorf("getEnv(\"%s\", \"MacOS\") FAILED, expected \"%s\" but got value \"%s\"", key, expectedVal, actualVal)
	}
}

func TestGetEnvAsInt(t *testing.T) {
	key := "R@NDO3_43N0KP(*I9jK_STR_INT_ENV"
	expectedVal := 300
	os.Setenv(key, strconv.Itoa(expectedVal))

	actualVal := getEnvAsInt(key, -10)

	if actualVal != expectedVal {
		t.Errorf("getEnv(\"%s\", \"M acOS\") FAILED, expected \"%d\" but got value \"%d\"", key, expectedVal, actualVal)
	}
}

func TestGetEnvAsIntAndGotDefault(t *testing.T) {
	key := "R@NDO3_43N0KP(*I9jK_STR_INT_ENV"
	os.Unsetenv(key)
	expectedVal := -10

	actualVal := getEnvAsInt(key, expectedVal)

	if actualVal != expectedVal {
		t.Errorf("getEnv(\"%s\", \"M acOS\") FAILED, expected \"%d\" but got value \"%d\"", key, expectedVal, actualVal)
	}
}

func TestDBConfig(t *testing.T) {
	dbHost := "test.acommerce-db.example"
	dbPort := 1234
	dbName := "example"
	dbUser := "ENG"
	dbPassword := "xxxxx"
	os.Setenv("DB_HOST", dbHost)
	os.Setenv("DB_PORT", strconv.Itoa(dbPort))
	os.Setenv("DB_NAME", dbName)
	os.Setenv("DB_USER", dbUser)
	os.Setenv("DB_PASSWORD", dbPassword)

	config := NewConfig()

	if config.DB.Host != dbHost {
		t.Errorf("config.DB.Host should return %s", dbHost)
	}
	if config.DB.Port != dbPort {
		t.Errorf("config.DB.Port should return %d", dbPort)
	}
	if config.DB.Name != dbName {
		t.Errorf("config.DB.Name should return %s", dbName)
	}
	if config.DB.Username != dbUser {
		t.Errorf("config.DB.Username should return %s", dbUser)
	}
	if config.DB.Password != dbPassword {
		t.Errorf("config.DB.Password should return %s", dbPassword)
	}
}
