package config

import (
	"os"
	"strconv"
)

// DBConfig holds a configuration for Application Database
type DBConfig struct {
	Host     string
	Port     int
	Name     string
	Username string
	Password string
}

// Config holds a configuration for the whole Application
type Config struct {
	DB DBConfig
}

// New returns a new Config struct
func NewConfig() *Config {
	return &Config{
		DB: DBConfig{
			Host:     getEnv("DB_HOST", ""),
			Port:     getEnvAsInt("DB_PORT", 5432),
			Name:     getEnv("DB_NAME", ""),
			Username: getEnv("DB_USER", ""),
			Password: getEnv("DB_PASSWORD", ""),
		},
	}
}

func getEnv(key string, defaultVal string) string {
	val, exists := os.LookupEnv(key)

	if exists {
		return val
	}
	return defaultVal
}

func getEnvAsInt(key string, defaultVal int) int {
	val := getEnv(key, "")
	valInt, err := strconv.Atoi(val)

	if err == nil {
		return valInt
	}
	return defaultVal
}
