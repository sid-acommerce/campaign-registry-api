package datastore

import (
	"bitbucket.org/sid-acommerce/campaign-registry-api/internal/config"
	"database/sql"
	"fmt"
)

// NewDB returns DB connection.
func NewDB(cfg config.DBConfig) *sql.DB {
	psqlInfo := fmt.Sprintf("host=%s user=%s "+
		"password=%s dbname=%s sslmode=disable",
		cfg.Host, cfg.Username, cfg.Password, cfg.Name)

	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		panic(err)
	}

	return db
}
