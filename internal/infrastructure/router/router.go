package router

import (
	"bitbucket.org/sid-acommerce/campaign-registry-api/internal/interface/controller"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

// NewRouter returns an instance of Echo.
func NewRouter(e *echo.Echo, c *controller.AppController) *echo.Echo {
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	apiV1 := e.Group("api/v1")
	{
		campaigns := apiV1.Group("/campaigns")
		{
			campaigns.GET("", func(context echo.Context) error {
				return c.Campaign.GetCampaigns(context)
			})
		}

		activities := apiV1.Group("/activities")
		{
			activities.GET("", func(context echo.Context) error {
				return c.Activity.GetActivities(context)
			})
		}
	}

	return e
}
