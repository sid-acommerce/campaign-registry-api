package model

// CampaignList list campaign struct
type CampaignList struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

// Campaign basic campaign struct
type Campaign struct {
	ID          string `json:"id"`
	Name        string `json:"name"`
	Target      string `json:"target"`
	Description string `json:"description"`
}
