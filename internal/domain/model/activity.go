package model

type Activity struct {
	ID          string   `json:"id"`
	Name        string   `json:"name"`
	Description string   `json:"description"`
	Types       []string `json:"types"`
	Level       string   `json:"level"`
	LevelName   string   `json:"levelName,omitempty"`
}
