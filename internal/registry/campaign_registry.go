package registry

import (
	"bitbucket.org/sid-acommerce/campaign-registry-api/internal/campaign"
)

func (r *registry) NewCampaignController() campaign.CampaignController {
	return campaign.NewCampaignController(r.NewCampaignUsecase())
}

func (r *registry) NewCampaignUsecase() campaign.CampaignUsecase {
	return campaign.NewCampaignUsecase(r.NewCampaignRepository())
}

func (r *registry) NewCampaignRepository() campaign.CampaignRepository {
	return campaign.NewCampaignRepository(r.db)
}

// func (r *registry) NewCampaignPresenter() campaign.CampaignPresenter {
// 	return campaign.NewCampaignPresenter()
// }
