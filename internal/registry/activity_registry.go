package registry

import (
	"bitbucket.org/sid-acommerce/campaign-registry-api/internal/activity"
)

func (r *registry) NewActivityController() activity.ActivityController {
	return activity.NewActivityController(r.NewActivityUsecase())
}

func (r *registry) NewActivityUsecase() activity.ActivityUsecase {
	return activity.NewActivityUsecase(r.NewActivityRepository())
}

func (r *registry) NewActivityRepository() activity.ActivityRepository {
	return activity.NewActivityRepository(r.db)
}

// func (r *registry) NewCampaignPresenter() campaign.CampaignPresenter {
// 	return campaign.NewCampaignPresenter()
// }
