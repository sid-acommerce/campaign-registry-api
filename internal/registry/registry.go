package registry

import (
	"database/sql"

	"bitbucket.org/sid-acommerce/campaign-registry-api/internal/interface/controller"
)

// Registry acts as a place to resolve dependency.
type Registry interface {
	NewAppController() *controller.AppController
}

type registry struct {
	db *sql.DB
}

// NewRegistry returns an instance of Registry
func NewRegistry(db *sql.DB) Registry {
	return &registry{db}
}

func (r *registry) NewAppController() *controller.AppController {
	return &controller.AppController{
		Campaign: r.NewCampaignController(),
		Activity: r.NewActivityController(),
	}
}
