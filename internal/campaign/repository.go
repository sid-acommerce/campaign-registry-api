package campaign

import (
	"context"
	"database/sql"

	"bitbucket.org/sid-acommerce/campaign-registry-api/internal/domain/model"
	"bitbucket.org/sid-acommerce/campaign-registry-api/internal/infrastructure/datastore/models"
)

type campaignRepository struct {
	db *sql.DB
}

type CampaignRepository interface {
	FindAll() ([]*model.Campaign, error)
}

func NewCampaignRepository(db *sql.DB) CampaignRepository {
	return &campaignRepository{db}
}

func (r *campaignRepository) FindAll() ([]*model.Campaign, error) {
	campaigns, err := models.Campaigns().All(context.Background(), r.db)

	if err != nil {
		return nil, err
	}

	list := make([]*model.Campaign, len(campaigns))
	for i := 0; i < len(campaigns); i++ {
		campaign := campaigns[i]
		list[i] = &model.Campaign{campaign.CampaignID, campaign.Name, campaign.Target.String, campaign.Description.String}
	}

	return list, nil
}
