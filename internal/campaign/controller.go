package campaign

import (
	"bitbucket.org/sid-acommerce/campaign-registry-api/internal/domain/model"
	"github.com/labstack/echo/v4"
	"net/http"
)

type campaignController struct {
	campaignUsecase CampaignUsecase
}

type CampaignController interface {
	GetCampaigns(c echo.Context) error
}

func NewCampaignController(cu CampaignUsecase) CampaignController {
	return &campaignController{cu}
}

func (cc *campaignController) GetCampaigns(c echo.Context) error {
	var campaigns []*model.Campaign

	campaigns, err := cc.campaignUsecase.AllCampaigns()
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, campaigns)
}
