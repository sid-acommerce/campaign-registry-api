package campaign

import "bitbucket.org/sid-acommerce/campaign-registry-api/internal/domain/model"

type campaignPresenter struct {
}

type CampaignPresenter interface {
	ResponseCampaigns(rp []*model.Campaign) []*model.Campaign
}

func NewCampaignPresenter() CampaignPresenter {
	return &campaignPresenter{}
}

func (cp *campaignPresenter) ResponseCampaigns(rp []*model.Campaign) []*model.Campaign {
	return rp
}
