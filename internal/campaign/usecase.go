package campaign

import (
	"bitbucket.org/sid-acommerce/campaign-registry-api/internal/domain/model"
)

type CampaignUsecase interface {
	AllCampaigns() ([]*model.Campaign, error)
}

type campaignUsecase struct {
	campaignRepo CampaignRepository
}

func NewCampaignUsecase(cr CampaignRepository) CampaignUsecase {
	return &campaignUsecase{
		campaignRepo: cr,
	}
}

func (cr *campaignUsecase) AllCampaigns() ([]*model.Campaign, error) {
	campaigns, err := cr.campaignRepo.FindAll()

	return campaigns, err
}
