package activity

import (
	"bitbucket.org/sid-acommerce/campaign-registry-api/internal/domain/model"
)

type ActivityUsecase interface {
	AllActivities() ([]*model.Activity, error)
}

type activityUsecase struct {
	activityRepo ActivityRepository
}

func NewActivityUsecase(cr ActivityRepository) ActivityUsecase {
	return &activityUsecase{
		activityRepo: cr,
	}
}

func (cr *activityUsecase) AllActivities() ([]*model.Activity, error) {
	activities, err := cr.activityRepo.FindAll()

	return activities, err
}
