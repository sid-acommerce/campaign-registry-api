package activity

import (
	"context"
	"database/sql"

	"bitbucket.org/sid-acommerce/campaign-registry-api/internal/domain/model"
	"bitbucket.org/sid-acommerce/campaign-registry-api/internal/infrastructure/datastore/models"
)

type activityRepository struct {
	db *sql.DB
}

type ActivityRepository interface {
	FindAll() ([]*model.Activity, error)
}

func NewActivityRepository(db *sql.DB) ActivityRepository {
	return &activityRepository{db}
}

func (r *activityRepository) FindAll() ([]*model.Activity, error) {
	activities, err := models.Activities().All(context.Background(), r.db)

	if err != nil {
		return nil, err
	}

	list := make([]*model.Activity, len(activities))
	for i := 0; i < len(activities); i++ {
		activity := activities[i]
		list[i] = &model.Activity{activity.ActivityID, activity.Name, activity.Description.String, activity.Types, activity.Level, activity.LevelName.String}
	}

	return list, nil
}
