package activity

import (
	"bitbucket.org/sid-acommerce/campaign-registry-api/internal/domain/model"
	"github.com/labstack/echo/v4"
	"net/http"
)

type activityController struct {
	activityUsecase ActivityUsecase
}

type ActivityController interface {
	GetActivities(c echo.Context) error
}

func NewActivityController(au ActivityUsecase) ActivityController {
	return &activityController{au}
}

func (ac *activityController) GetActivities(c echo.Context) error {
	var activities []*model.Activity

	activities, err := ac.activityUsecase.AllActivities()
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, activities)
}
