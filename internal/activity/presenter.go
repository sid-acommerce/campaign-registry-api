package activity

import "bitbucket.org/sid-acommerce/campaign-registry-api/internal/domain/model"

type activityPresenter struct {
}

type ActivitiyPresenter interface {
	ResponseActivities(rp []*model.Activity) []*model.Activity
}

func NewActivityPresenter() ActivitiyPresenter {
	return &activityPresenter{}
}

func (cp *activityPresenter) ResponseActivities(rp []*model.Activity) []*model.Activity {
	return rp
}
